﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DemoClass01
{
    /// <summary>
    /// 测试DemoClass01
    /// </summary>
    [RoutePrefix("demo")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DemoController : ApiController
    {
        /// <summary>
        /// 测试方法1
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("get1")]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse("demo class 01 response");
        }
    }
}
