﻿using IISHost.Areas.HelpPage.ModelDescriptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;
using System.Web.Http.Controllers;
using System.IO;
using System.Reflection;

namespace IISHost.Areas.HelpPage
{
    public class MultiXmlDocumentationProvider : IDocumentationProvider, IModelDocumentationProvider
    {
        private IList<XmlDocumentationProvider> _documentationProviders;
        public MultiXmlDocumentationProvider(string xmlDocFilesPath)
        {
            _documentationProviders = new List<XmlDocumentationProvider>();

            foreach (string file in Directory.GetFiles(xmlDocFilesPath, "*.xml"))
            {
                _documentationProviders.Add(new XmlDocumentationProvider(file));
            }
        }

        public string GetDocumentation(MemberInfo member)
        {
            return _documentationProviders.Select(x => x.GetDocumentation(member)).FirstOrDefault(x => !string.IsNullOrEmpty(x));
        }

        public string GetDocumentation(Type type)
        {
            return _documentationProviders.Select(x => x.GetDocumentation(type)).FirstOrDefault(x => !string.IsNullOrEmpty(x));
        }

        public string GetDocumentation(HttpParameterDescriptor parameterDescriptor)
        {
            return _documentationProviders.Select(x => x.GetDocumentation(parameterDescriptor)).FirstOrDefault(x => !string.IsNullOrEmpty(x));
        }

        public string GetDocumentation(HttpActionDescriptor actionDescriptor)
        {
            return _documentationProviders.Select(x => x.GetDocumentation(actionDescriptor)).FirstOrDefault(x => !string.IsNullOrEmpty(x));
        }

        public string GetDocumentation(HttpControllerDescriptor controllerDescriptor)
        {
            return _documentationProviders.Select(x => x.GetDocumentation(controllerDescriptor)).FirstOrDefault(x => !string.IsNullOrEmpty(x));
        }

        public string GetResponseDocumentation(HttpActionDescriptor actionDescriptor)
        {
            return _documentationProviders.Select(x => x.GetDocumentation(actionDescriptor)).FirstOrDefault(x => !string.IsNullOrEmpty(x));
        }
    }
}