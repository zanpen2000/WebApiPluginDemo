﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http.Dispatcher;

namespace IISHost.App_Start
{
    public class PluginsAssembliesResolver : DefaultAssembliesResolver
    {
        public override ICollection<Assembly> GetAssemblies()
        {
            ICollection<Assembly> baseAssemblies = base.GetAssemblies();

            var mp = HttpContext.Current.Server.MapPath(WebSettingsConfig.plugin_location);

            foreach (string file in Directory.GetFiles(mp, "*.dll"))
            {
                var controllersAssembly = Assembly.LoadFile(file);
                baseAssemblies.Add(controllersAssembly);
            }

            List<Assembly> assemblies = new List<Assembly>(baseAssemblies);
            return assemblies;
        }
    }
}