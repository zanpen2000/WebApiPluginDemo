﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace democlass02.Controllers
{
    [RoutePrefix("demo")]
    public class SystemController : ApiController
    {
        
        [HttpGet,Route("get2")]
        public string Get()
        {
            return "system controller";
        }
    }
}
